use std::{collections::HashMap};
type Cell = i64;

#[derive(Debug,Clone)]
#[repr(u8)]
enum InstT {
    PUSH,
    POP,
    ADD,
    SUB,
    MOV,
    JMP,
    JMPE,
    JMPL,
    JMPG,
    CMP,
    WRT,
    HALT
}

#[derive(Debug,Clone)]
#[repr(u8)]
enum OP {
    VALUE(i64),
    MEMORYAT(Box<OP>),
    REGISTER(i64),
    SUM((Box<OP>,Box<OP>)),
    SUB((Box<OP>,Box<OP>)),
}


enum RES {
    EQ = 1 << 0,
    GT = 1 << 1,
    LT = 1 << 2 
}
#[allow(dead_code)]
#[repr(usize)]
enum REG {
    R0,
    R1,
    R2,
    R3,
    R4,
    R5,
    R6,
    R7,
    RES,
    IP,
    SP,
}
impl OP {
    fn r(op:i64) -> Self {
        OP::REGISTER(op)
    }
    fn v(op:i64) -> Self {
        OP::VALUE(op)
    }
    fn memory_at(op:OP) -> Self {
        OP::MEMORYAT(Box::new(op))
    }
    fn sum(op:OP,op2:OP) -> Self {
        OP::SUM((Box::new(op),Box::new(op2)))
    }
    fn sub(op:OP,op2:OP) -> Self {
        OP::SUB((Box::new(op),Box::new(op2)))
    }
    fn to_str(&self) -> String {
        let s = match self {
            OP::VALUE(v) => v.to_string(),
            OP::REGISTER(v) => {
                match v {
                    0..=7   => format!("r{}",v.to_string()),
                    9       => "ip".to_string(),
                    10      => "sp".to_string(),
                    _=> panic!("unknow regiser")
                }
            },
            OP::MEMORYAT(op) => format!("[{}]",op.to_str()),
            OP::SUM((op,op1)) => {
                format!("{}+{}",op.to_str(),op1.to_str())
            },
            OP::SUB((op,op1)) => {
                format!("{}-{}",op.to_str(),op1.to_str())
            }
        };
        s.to_string()

    } 
    fn from_str(op:&str,points:&HashMap<&str,i64>) -> Result<Self,String> {
        if op.len() == 0 {
            Err(format!("operand '{}' couldnt be parsed",op))
        }else if op.len() > 2 && op.starts_with("[") && op.ends_with("]") {
            Ok(OP::memory_at(OP::from_str(&op.replace("]", "").replace("[", ""),points)?)) 
        }else if op.contains("-") {
            let parts = op.split("-").collect::<Vec<&str>>();
            Ok(OP::sub(OP::from_str(parts[0], points)?,
                OP::from_str(parts[1], points)?))
        }else if op.contains("+") {
            let parts = op.split("+").collect::<Vec<&str>>();
            Ok(OP::sum(OP::from_str(parts[0], points)?,
                OP::from_str(parts[1], points)?))
        }else if op.len() == 2 && op.chars().nth(0).unwrap() == 'r' && op.chars().nth(1).unwrap().is_digit(10) {
            let r = op.chars().nth(1).unwrap().to_digit(10).unwrap();
            match r {
                0..=7   => Ok(OP::r(r as i64)),
                _       => Err(format!("unknown register r{}",r))
            }
            
        }else if op.len() == 2 && op == "sp" {
            Ok(OP::r(REG::SP as i64)) 
        }else if op.len() == 2 && op == "ip" {
            Ok(OP::r(REG::IP as i64)) 
        }else if op.len() > 1 && op.chars().nth(0).unwrap() == '@' {
            Ok(OP::v(*points.get(&op[1..]).unwrap())) 
        }else if op.to_string().is_numeric(10) {
             Ok(OP::v(i64::from_str_radix(op,10).unwrap()))
        }else {
            Err(format!("operand: {} couldnt be parsed",op))
        }
    }
}
#[derive(Debug,Clone)]
struct INST {
    itype:InstT,
    operands:Vec<OP>
}

impl INST {
    fn new(itype:InstT,operands:Vec<OP>) -> Self {
        INST {itype,operands}
    }
    fn push(op:OP) -> Self {
       INST::new(InstT::PUSH,vec![op])
    } 
    fn pop(op:OP) -> Self {
       INST::new(InstT::POP,vec![op]) 
    }
    fn add(src:OP,dest:OP) -> Self {
       INST::new(InstT::ADD,vec![src,dest]) 
    }
    fn sub(src:OP,dest:OP) -> Self {
        INST::new(InstT::SUB,vec![src,dest])
    }
    fn halt() -> Self {
        INST::new(InstT::HALT,vec![])
    }
    fn mov(src:OP,dest:OP) -> Self {
        INST::new(InstT::MOV,vec![src,dest])
    }
    fn jmp(dest:OP) -> Self {
        INST::new(InstT::JMP,vec![dest])
    }
    fn jmpe(dest:OP) -> Self {
        INST::new(InstT::JMPE,vec![dest])
    }
    fn jmpl(dest:OP) -> Self {
        INST::new(InstT::JMPL,vec![dest])
    }
    fn jmpg(dest:OP) -> Self {
        INST::new(InstT::JMPG,vec![dest])
    }
    fn cmp(src:OP,dest:OP) -> Self {
        INST::new(InstT::CMP,vec![src,dest])
    }
    fn wrt(src:OP) -> Self {
        INST::new(InstT::WRT,vec![src])
    }
    fn from_str(inst:&str,operands:Vec<OP>) -> Result<Self,String> {
        match inst {
            "push"  => {
                if operands.len() != 1 {
                    Err("pop takes one operand".to_string()) 
                }else {
                    Ok(INST::push(operands[0].clone()))
                }
            }
            "pop"  => {
                if operands.len() != 1 {
                    Err("pop takes one operand".to_string()) 
                }else {
                    Ok(INST::pop(operands[0].clone()))
                }
            }
            "sub"  => {
                if operands.len() != 2 {
                    Err("sub takes two operands".to_string()) 
                }else {
                    Ok(INST::sub(operands[0].clone(),operands[1].clone()))
                }
            }
            "add"  => {
                if operands.len() != 2 {
                    Err("add takes two operands".to_string()) 
                }else {
                    Ok(INST::add(operands[0].clone(),operands[1].clone()))
                }
            }
            "mov"  => {
                if operands.len() != 2 {
                    Err("add takes two operands".to_string()) 
                }else {
                    Ok(INST::mov(operands[0].clone(),operands[1].clone()))
                }
            }
            "jmp"  => {
                if operands.len() != 1 {
                    Err("jmp takes one operands".to_string()) 
                }else {
                    Ok(INST::jmp(operands[0].clone()))
                }
            }
            "jmpe"  => {
                if operands.len() != 1 {
                    Err("jmpe takes one operand".to_string()) 
                }else {
                    Ok(INST::jmpe(operands[0].clone()))
                }
            }
            "jmpl"  => {
                if operands.len() != 1 {
                    Err("jmpl takes one operand".to_string()) 
                }else {
                    Ok(INST::jmpl(operands[0].clone()))
                }
            }
            "jmpg"  => {
                if operands.len() != 1 {
                    Err("jmpg takes one operand".to_string()) 
                }else {
                    Ok(INST::jmpg(operands[0].clone()))
                }
            }
            "cmp"  => {
                if operands.len() != 2 {
                    Err("cmp takes two operands".to_string()) 
                }else {
                    Ok(INST::cmp(operands[0].clone(),operands[1].clone()))
                }
            }
            "wrt"  => {
                if operands.len() != 1 {
                    Err("wrt takes one operand".to_string()) 
                }else {
                    Ok(INST::wrt(operands[0].clone()))
                }
            }
            "halt"  => {
                if operands.len() != 0 {
                    Err("halt takes 0 operands".to_string()) 
                }else {
                    Ok(INST::halt())
                }
            }
            _       => {
                Err(format!("unknown instruction: {}",inst))
            }
        }
    }

}

struct Vm {
    stack:Vec<Cell>,
    program:Vec<INST>,
    registers:[i64;11],
    halt:bool
}
impl Vm {
    fn new() -> Self {
        Vm { stack: Vec::new(), program: Vec::new(), registers:[0;11],halt:false }
    }
    fn execute_inst(&mut self){
        assert!((self.registers[REG::IP as usize] as usize) < self.program.len(),"illigal instruction access");
        let inst = self.program[self.registers[REG::IP as usize] as usize].clone(); 
        self.registers[REG::IP as usize] += 1;
        match inst.itype {
            InstT::PUSH  => {
                let op = inst.operands.first().unwrap();
                let v = self.get_operand(op);
                self.stack.push(v);
                self.registers[REG::SP as usize] = self.stack.len() as i64 - 1;
           }
           InstT::POP   => {
                let op = inst.operands.first().unwrap();
                let v = self.stack.pop().unwrap();
                self.set_operand(op,v);
                self.registers[REG::SP as usize] = self.stack.len() as i64 - 1;
           }
           InstT::ADD   => {
                let op = inst.operands.first().unwrap();
                let op2 = inst.operands.last().unwrap();
                self.set_operand(op2,self.get_operand(op) + self.get_operand(op2)); 
           }
           InstT::SUB   => {
                let op = inst.operands.first().unwrap();
                let op2 = inst.operands.last().unwrap();
                self.set_operand(op2,self.get_operand(op2) - self.get_operand(op)); 

           }
           InstT::MOV   => {
                let op = inst.operands.first().unwrap();
                let op2 = inst.operands.last().unwrap();
                self.set_operand(op2,self.get_operand(op)); 

           }
           InstT::JMP   => {
                let op = inst.operands.first().unwrap();
                self.registers[REG::IP as usize] = self.get_operand(op);
           }
           InstT::JMPE   => {
                let op = inst.operands.first().unwrap();
                if self.registers[REG::RES as usize] == RES::EQ as i64 {
                    self.registers[REG::IP as usize] = self.get_operand(op);
                }
           }
           InstT::JMPL   => {
                let op = inst.operands.first().unwrap();
                if self.registers[REG::RES as usize] == RES::LT as i64 {
                    self.registers[REG::IP as usize] = self.get_operand(op);
                }
           }
           InstT::JMPG   => {
                let op = inst.operands.first().unwrap();
                if self.registers[REG::RES as usize] == RES::GT as i64 {
                    self.registers[REG::IP as usize] = self.get_operand(op);
                }
           }
           InstT::CMP   => {
                let op = inst.operands.first().unwrap();
                let op2 = inst.operands.last().unwrap();
                if self.get_operand(op) == self.get_operand(op2) {
                    self.registers[REG::RES as usize] = RES::EQ as i64;
                }else if self.get_operand(op) < self.get_operand(op2) {
                    self.registers[REG::RES as usize] = RES::LT as i64;
                }else{
                    self.registers[REG::RES as usize] = RES::GT as i64;
                }
           }
            InstT::WRT   => {
                let op = inst.operands.first().unwrap();
                //std::io::stdout().write(self.get_operand(op));
                println!("{}",self.get_operand(op));
           }
           InstT::HALT  => {
                self.halt = true;
           }
        }; 
    }
    fn _debug_execute(&mut self) {
        let mut limit = 10;
        while !self.halt && limit != 0 {
            let inst = self.program[self.registers[REG::IP as usize] as usize].clone(); 
            println!("AFTER {:?}:",inst);
            self.execute_inst();
            println!("STACK:");
            for v in &self.stack {
                println!("{}",v);
            }
            println!("REGISTERS: ");
            for v in &self.registers {
                println!("{}",v);
            }
            println!();
            limit -=1;
        }
    }
    fn execute(&mut self) {
        let mut limit = 100000;
        while !self.halt && limit != 0 {
            self.execute_inst();
            limit -=1;
        }
    }
    
    fn get_operand(&self,op:&OP) -> i64 {
        match op {
            OP::VALUE(v) => *v,
            OP::REGISTER(v) => self.registers[*v as usize],
            OP::MEMORYAT(op) => {
                let add = self.get_operand(op);
                self.stack[add as usize]
            },
            OP::SUM((op1,op2)) => {
                self.get_operand(op1) + self.get_operand(op2)
            },
            OP::SUB((op1,op2)) => {
                self.get_operand(op1) - self.get_operand(op2)
            }
        }

    }
    fn set_operand(&mut self,op:&OP,value:i64)  {
        match op {
            OP::REGISTER(v) => self.registers[*v as usize] = value,
            OP::MEMORYAT(op) => {
                let add = self.get_operand(op);
                self.stack[add as usize] = value;
            },
            _=> panic!("Error: operand type cannot receive value"),
        };

    }
    fn _dump_stack(&self) {
        println!("STACK:");
        for v in &self.stack {
            println!("\t{}",v);
        }
    }
    fn _dump_registers(&self) {
        println!("REGISTERS:");
        for (i,v) in self.registers.iter().enumerate() {
            println!("\tr{}: {}",i+1,v);
        }
    } 
    fn _dump(&self) {
        self._dump_stack();
        self._dump_registers();
        println!();
    }
}
struct InstLine{
    line:i64,
    inst:String,
    operands:Vec<String>
}
trait IsNumeric {
    fn is_numeric(&self,radix:u32) -> bool;
}
impl IsNumeric for String {
    fn is_numeric(&self,radix:u32) -> bool {
        for (i,c) in self.chars().enumerate() {
            if !(c.is_digit(radix) ||( i == 0 && (c == '+' || c == '-'))){
               return false; 
            }
        }
        true
    }
}


fn load_sasm(name:&str) -> Vec<INST> {
    let input = std::fs::read_to_string(name).unwrap();
    let mut lines = input.lines();
    let mut line_num = 0;
    let mut points = HashMap::<&str,i64>::new();
    let mut instructions:Vec<InstLine> = vec![];
    while let Some(mut line) = lines.next() {
        line_num += 1;
        line = line.trim(); 
        if line.is_empty() {
            continue;
        }
        if line[0..1] == *"@" {
            points.insert(&line[1..],instructions.len() as i64);
            continue;
        }
        let inst = line.split_whitespace().collect::<Vec<&str>>()[0].to_owned();
        let mut remaining = line.to_owned();
        remaining = remaining.replace(&inst, "").trim().to_owned();
        let mut operands :Vec<String>=vec![];
        if remaining.len() > 0 { 
            remaining.split(',')
            .for_each(|op| operands.push(op.trim().to_owned()));
        }
        instructions.push(InstLine { line: line_num, inst, operands});
    }
    let mut program:Vec<INST> = vec![];
    for i in instructions {
        let operands = i.operands.iter().map(|op| 
            match OP::from_str(op,&points) {
                Ok(op) => op,
                Err(err) => {
                eprintln!("Error: {}: {}",i.line,err);
                std::process::exit(-1);
            }})
            .collect::<Vec<OP>>(); 
        match INST::from_str(&i.inst,operands) {
            Ok(inst) => program.push(inst),
            Err(err) => {
                eprintln!("Error: {}: {}",i.line,err);
                std::process::exit(-1);
            }
        }
    }
    program
}
fn save_sasm(name:&str,program:&Vec<INST>) {
    let mut sasm = String::new();
    for inst in program {
        sasm += &format!("{:?}",inst.itype).to_lowercase(); 
        sasm += "\t";
        for (i,o) in inst.operands.iter().enumerate() {
            sasm += &o.to_str(); 
            if i != inst.operands.len() -1  {
                sasm += ", "
            }
        }
        sasm += "\n";
    }
    std::fs::write(name, sasm).unwrap();
}
fn serialize_operand(op:OP) -> Vec<u8> {
    let mut bytes = vec![];
    match op {
        OP::VALUE(v) => {
            bytes.push(0);
            bytes.extend(v.to_le_bytes());
        },
        OP::MEMORYAT(op) => {
            bytes.push(1);
            bytes.extend(serialize_operand(*op)); 
        },
        OP::REGISTER(v) => {
            bytes.push(2);
            bytes.extend(v.to_le_bytes());
        },
        OP::SUM((op,op2)) => {
            bytes.push(3);
            bytes.extend(serialize_operand(*op)); 
            bytes.extend(serialize_operand(*op2)); 

        },
        OP::SUB((op,op2)) => {
            bytes.push(4);
            bytes.extend(serialize_operand(*op)); 
            bytes.extend(serialize_operand(*op2)); 
        },
    } 
    bytes
}
fn deserialize_operand(bytes:&Vec<u8>,pos:&mut usize) -> OP {
    match bytes[*pos]  {
        0 => {
            *pos += 1;
            let op = OP::v(i64::from_le_bytes(bytes[*pos..*pos+8].try_into().unwrap()));
            *pos += 8;
            op
        },
        1 => {
            *pos += 1;
            OP::memory_at(deserialize_operand(bytes,pos))
        },
        2 => {
            *pos += 1;
            let op = OP::r(i64::from_le_bytes(bytes[*pos..*pos+8].try_into().unwrap()));
            *pos += 8;
            op
        },
        3 => {
            *pos += 1;
            OP::sum(deserialize_operand(bytes, pos), deserialize_operand(bytes, pos))
        },
        4 => {
            *pos += 1;
            OP::sub(deserialize_operand(bytes, pos), deserialize_operand(bytes, pos))
        },
        _=> panic!("unknown operand value {}",bytes[*pos])
    } 
}
fn save_program(name:&str,program:&Vec<INST>) {
   let mut bytes:Vec<u8> = vec![];
   for inst in program{
        bytes.push(inst.itype.clone() as u8);
        bytes.push((inst.operands.len()) as u8);
        for op in &inst.operands {
            bytes.extend(serialize_operand(op.clone()));
        }
   }
   std::fs::write(name, bytes).unwrap();
}
fn load_program(name:&str) -> Vec<INST> {
    let bytes = std::fs::read(name).unwrap();
    let mut program: Vec<INST> = vec![];
    let mut pos = 0;
    while pos < bytes.len() -1 {
        let inst:InstT= unsafe {std::intrinsics::transmute(bytes[pos])};
        pos += 1;
        let mut operands:Vec<OP> = vec![];
        let len = bytes[pos];
        pos += 1;
        for _ in 0..len {
            operands.push(deserialize_operand(&bytes, &mut pos));
        }
        program.push(INST::new(inst,operands));
    }
    program
}
fn main() {
    let args:Vec<String> = std::env::args().collect();
    if args.len() < 2 {
        eprintln!("USAGE: {} run/ass <args>",args[0]);
        std::process::exit(0);
    }
    match args[1].as_str() {
       "ass" => {
            if args.len() != 4 {
                eprintln!("USAGE: {} ass <in-file> <out-file>",args[0]);
                std::process::exit(0);
            }
            save_program(&args[3],&load_sasm(&args[2]));
       },
       "run" => {
            if args.len() != 3 {
                eprintln!("USAGE: {} run <in-file>",args[0]);
                std::process::exit(0);
            }
            if args[2].ends_with(".sasm") {
                let mut vm = Vm::new();
                vm.program = load_sasm(&args[2]);
                vm.execute();
            }else if args[2].ends_with(".svm"){
                let mut vm = Vm::new();
                vm.program = load_program(&args[2]);
                vm.execute();
            }else {
                eprintln!("USAGE: {} run/ass <args>",args[0]);
                std::process::exit(0);
            }
           
       },
       "dis" => {
            if args.len() != 4 {
                eprintln!("USAGE: {} dis <in-file> <out-file>",args[0]);
                std::process::exit(0);
            }
            save_sasm(&args[3],&load_program(&args[2]));
       }
       _ => {
            eprintln!("USAGE: {} run/ass <args>",args[0]);
            std::process::exit(0);
 
       }
    }
}
